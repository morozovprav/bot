<?php

namespace App\Http\Controllers;

use App\Models\TelegramAccount;
use App\Models\User;
use Hu\MadelineProto\Facades\Factory;
use Hu\MadelineProto\Facades\MadelineProto;
use Hu\MadelineProto\Factories\MadelineProtoFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    /**
     * @var MadelineProtoFactory
     */
    private $factory;

    public function __construct(MadelineProtoFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Handle new telegram account session.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newSession($request)
    {

        //Insert the session into the telegram_sessions table
        $user = User::find(Auth::id());

        $telegramSession = $user->telegramSessions()->create([
            'session_file' => "{$user->name}{$request->phone}.madeline",
            'telegram_account_id' => $request->id,
        ]);

        //You can either use one of this following method

        $madelineProto = $this->factory->get($telegramSession);

        $madelineProto->phoneLogin($request->phone);

        return response()->json([
            'message' => 'Phone code sent!'
        ]);
    }

    /**
     * Handle submit telegram account login code.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmCode($fields, $model) {

        $user = User::find(Auth::id());

        $telegramSession = $user->telegramSessions()->where('telegram_account_id', $model->id)->firstOrFail();

        $madelineProto = Factory::get($telegramSession);

        $madelineProto->completePhoneLogin($fields->code);

        $models = TelegramAccount::find($model->id);
        $models->session_exist = 1;
        $models->save();

        return response()->json([
            'message' => 'Account logged in!'
        ]);
    }

    public function testQuery() {

//        $user = User::find(1);
//
//        $telegramSession = $user->telegramSessions()->first();

        $madelineProto = $this->factory->make('Mrz.madeline');
        $r = $madelineProto->getClient();
//        $r = MadelineProto::getClient();
//
        $Chats = $r->messages->getAllChats('chat')['chats'];
        dd($Chats);
//        $Chats = array_slice($Chats, 91, 93);
    }
}
