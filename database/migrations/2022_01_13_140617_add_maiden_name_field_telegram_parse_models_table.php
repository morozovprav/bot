<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaidenNameFieldTelegramParseModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table){
            $table->string('employee_maiden_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table){
            $table->dropColumn('employee_maiden_name');
        });
    }
}
