<?php

namespace App\Models;

use Hu\MadelineProto\Facades\Factory;
use Hu\MadelineProto\Facades\MadelineProto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Actions\Action;

class TelegramParseModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'chat_id';

//    public $incrementing = false;

    protected $table = 'telegram_parse_models';

    public $fillable = [
        'chat_title',
        'chat_id',
        'user_id',
        'first_name',
        'last_name',
        'username',
        'phone',

        'phone_matched',
        'full_name_matched',
        'employee_first_name',
        'employee_middle_name',
        'employee_last_name',
        'employee_phone',
        'employee_maiden_name',

    ];

    public static function getTelegramData($method, $params = [])
    {
        if (!empty($params)) {
            $url = env('TELEGRAM_FINAL_URL') . $method . '?' . http_build_query($params);
        } else {
            $url = env('TELEGRAM_FINAL_URL') . $method;
        }

        return json_decode(file_get_contents($url), JSON_OBJECT_AS_ARRAY);
    }

    public static function telegramDataProcessing($data)
    {
        $updateId = 0;
        $parseResult = [];
        foreach ($data as $value) {

            if (array_key_exists('message', $value)) {

                $updateId = $value['update_id'];

                if (array_key_exists('id', $value['message']['chat'])) {
                    $chatId = $value['message']['chat']['id'];
                } else {
                    continue;
                }

                if (array_key_exists('title', $value['message']['chat'])) {
                    $chatTitle = $value['message']['chat']['title'];
                } else {
                    $chatTitle = 'none';
                }

                $chatMemberId = $value['message']['from']['id'];

                if (array_key_exists('first_name', $value['message']['from'])) {
                    $chatMemberFirstName = $value['message']['from']['first_name'];
                } else {
                    $chatMemberFirstName = 'none';
                }

                if (array_key_exists('last_name', $value['message']['from'])) {
                    $chatMemberLastName = $value['message']['from']['last_name'];
                } else {
                    $chatMemberLastName = 'none';
                }

                if (array_key_exists('username', $value['message']['from'])) {
                    $chatMemberUserName = $value['message']['from']['username'];
                } else {
                    $chatMemberUserName = 'none';
                }

            } else {
                continue;
            }
            $row = [
                'user_id' => $chatMemberId,
                'first_name' => $chatMemberFirstName,
                'last_name' => $chatMemberLastName,
                'username' => $chatMemberUserName,
                'chat_id' => $chatId,
                'chat_title' => $chatTitle,
            ];

            $parseResult[] = $row;

        }

        $parseResult = array_unique($parseResult, SORT_REGULAR);
        return $parseResult;
    }

    public static function writeToDb($data, array $employees = [])
    {

        $count = 0;
        foreach ($data as $userData) {
            if (!TelegramParseModel::where('chat_id', $userData['chat_id'])->where('user_id', $userData['user_id'])->exists()
                /*&& $userData['first_name'] == 'Доскалов'*/) {

                $userData['phone_matched'] = false;
                $userData['full_name_matched'] = false;
                if ($userData['phone'] == 'none'){
                    $count += 1;
                    $newParse = new TelegramParseModel($userData);
                    $newParse->save();
                    continue;
                }

                foreach ($employees as $employee) {

                    if (!array_key_exists('firstName', $employee)){ continue;}
//                    if (!array_key_exists('middleName', $employee)){ continue;}

                    $employeeFirstName = mb_strtolower(str_ireplace('ё', 'е', $employee['firstName']));
//                    $employeeFirstPlusMiddleName = mb_strtolower($employee['firstName']) .' '. mb_strtolower($employee['middleName']);
                    $employeeLastName = mb_strtolower(str_ireplace('ё', 'е', $employee['lastName']));
                    if (array_key_exists('maidenName', $employee)){
                        $employeeMaidenName = mb_strtolower(str_ireplace('ё', 'е', $employee['maidenName']));
                    }


                    $userDataFirstName = mb_strtolower(str_ireplace('ё', 'е', $userData['first_name']));
                    $userDataLastName = mb_strtolower(str_ireplace('ё', 'е', $userData['last_name']));

                    $employeePhone = trim($employee['phone'], "+");
                    $userDataPhone = trim($userData['phone'], "+");

                    if (strpos($userDataPhone, $employeePhone) !== false) {

                        $userData['phone_matched'] = true;

                        if (array_key_exists('firstName', $employee)){
                            $userData['employee_first_name'] = $employee['firstName'];
                        }
                        if (array_key_exists('middleName', $employee)) {
                            $userData['employee_middle_name'] = $employee['middleName'];
                        }
                        if (array_key_exists('lastName', $employee)){
                            $userData['employee_last_name'] = $employee['lastName'];
                        }
                        $userData['employee_maiden_name'] = array_key_exists('maidenName', $employee) ? $employee['maidenName'] : null;
                        $userData['employee_phone'] = $employee['phone'];

// сравнение имен и фамилий в телеграме и в файле.
// версия алгоритма: 1
//                        if ((strpos($userDataFirstName, $employeeFirstPlusMiddleName) !== false || strpos($userDataFirstName, $employeeFirstName) !== false)
//                            && strpos($userDataLastName, $employeeLastName) !== false) {
//                            $userData['full_name_matched'] = true;
//                        }
// сравнение имен и фамилий в телеграме и в файле.
// версия алгоритма: 2
                        $firstNameMatch = strpos($userDataFirstName, $employeeFirstName) !== false || strpos($userDataLastName, $employeeFirstName) !== false;
                        $lastNameMatch = strpos($userDataFirstName, $employeeLastName) !== false || strpos($userDataLastName, $employeeLastName) !== false;
                        if (array_key_exists('maidenName', $employee)){
                            $maidenNameMatch = strpos($userDataFirstName, $employeeMaidenName) !== false || strpos($userDataLastName, $employeeMaidenName) !== false;
                            if ($firstNameMatch && ($lastNameMatch || $maidenNameMatch)){
                                $userData['full_name_matched'] = true;
                            }
                        } else {
                            if ($firstNameMatch && $lastNameMatch){
                                $userData['full_name_matched'] = true;
                            }
                        }
                        break;
                    }
                }
                $count += 1;
                $newParse = new TelegramParseModel($userData);
                $newParse->save();
            }

        }

        return $count;
    }

    public static function test(){

        $telegramRawData = self::getTelegramData('getUpdates')['result'];

        if (count($telegramRawData) < 2){
            return Action::danger('Нет данных из телеграм чатов!');
        }

        $data = [];
        while (count($telegramRawData) > 1){
            $data = array_merge($data, $telegramRawData);
            $updateId =  $telegramRawData[array_key_last($telegramRawData)]['update_id'];
            TelegramParseModel::getTelegramData('getUpdates', ['offset' => $updateId]);
            $telegramRawData = TelegramParseModel::getTelegramData('getUpdates')['result'];
        }
        $data = TelegramParseModel::telegramDataProcessing($data);

        $fileName = EmployeeListModel::query()->select('file')->firstOrFail();
        $employees = EmployeeListModel::getAndProccessingFile($fileName);

        $result = TelegramParseModel::writeToDb($data, $employees);

    }

    public static function getTelegramDataIntoApi($fileName, $onlyUniqUsers = false): array
    {
        $telegramAccount = Factory::make($fileName);

        $r = $telegramAccount->getClient();

        $chats = $r->messages->getAllChats()['chats'];

        $parseResult = [];

        foreach ($chats as $key => $chat) {

//if ($chat['_'] == 'chat'){continue;}

            $chatInfoAndUsers = [];
            if($chat['_'] == 'channel') {
//                if ($chat['_'] == 'channel'){continue;}
                //ПОлучаю полную информацию по юзерам в закрытой группе. Я админ в этойй же группе
                $chatInfoAndUsers = $r->getPwrChat('channel#' . $chat['id']);
//                $parseResult[] = $chat;

//                if (array_key_exists('participants', $chatInfoAndUsers) AND empty($chatInfoAndUsers['participants'])){continue;}


            } else
              if ($chat['_'] == 'chat') {
                $chatInfoAndUsers = $r->getPwrChat(-$chat['id']);
            }

            if (array_key_exists('id', $chatInfoAndUsers)){
                $chatId = $chatInfoAndUsers['id'];
            } else {continue;}


            if (array_key_exists('title', $chatInfoAndUsers)) {
                $chatTitle = $chatInfoAndUsers['title'];
            } else {
                $chatTitle = 'none';
            }

            if (array_key_exists('participants', $chatInfoAndUsers) AND !empty($chatInfoAndUsers['participants'])){


                foreach ($chatInfoAndUsers['participants'] as $user){



                    if (array_key_exists('first_name', $user['user'])) {
                        $chatMemberFirstName = $user['user']['first_name'];
                    } else {
                        $chatMemberFirstName = 'none';
                    }

                    if (array_key_exists('last_name', $user['user'])) {
                        $chatMemberLastName = $user['user']['last_name'];
                    } else {
                        $chatMemberLastName = 'none';
                    }

                    if (array_key_exists('username', $user['user'])) {
                        $chatMemberUserName =$user['user']['username'];
                    } else {
                        $chatMemberUserName = 'none';
                    }

                    if (array_key_exists('phone', $user['user'])) {
                        $chatMemberUserPhone =$user['user']['phone'];
                    } else {
                        $chatMemberUserPhone = 'none';
                    }

                    $chatMemberId = $user['user']['id'];

                    $row = [
                        'user_id' => $chatMemberId,
                        'first_name' => $chatMemberFirstName,
                        'last_name' => $chatMemberLastName,
                        'username' => $chatMemberUserName,
                        'phone' => $chatMemberUserPhone,
                        'chat_id' => $chatId,
                        'chat_title' => $chatTitle,
                    ];

                    if ($onlyUniqUsers){
                        $parseResult[$chatMemberId] = $row;
                    } else {
                        $parseResult[] = $row;
                    }
                }
            }

        }
//        dd($parseResult);
        return $parseResult;
    }

    public static function testMultiAccount()
    {

    }

}
