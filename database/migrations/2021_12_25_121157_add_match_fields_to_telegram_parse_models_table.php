<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMatchFieldsToTelegramParseModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table) {
            $table->tinyInteger('last_name_matched');
            $table->tinyInteger('first_name_matched');
            $table->tinyInteger('phone_matched');
            $table->tinyInteger('match_lvl');
            $table->dropColumn('exists_employees_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table) {
            $table->dropColumn('last_name_matched');
            $table->dropColumn('first_name_matched');
            $table->dropColumn('phone_matched');
            $table->dropColumn('match_lvl');
            $table->tinyInteger('exists_employees_list')->nullable();
        });
    }
}
