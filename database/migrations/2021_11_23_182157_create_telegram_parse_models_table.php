<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramParseModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_parse_models', function (Blueprint $table) {
            $table->string('chat_title')->nullable();
            $table->bigInteger('chat_id');
            $table->bigInteger('user_id');
            $table->primary(['chat_id', 'user_id'], 'pk_chat_id_user_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('username');
            $table->tinyInteger('exists_employees_list')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_parse_models');
    }
}
