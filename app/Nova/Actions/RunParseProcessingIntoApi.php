<?php

namespace App\Nova\Actions;

use App\Http\Controllers\TelegramParseController;
use App\Jobs\ProcessParse;
use App\Models\TelegramAccount;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use MongoDB\Driver\Exception\Exception;

class RunParseProcessingIntoApi extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Disables action log events for this action.
     *
     * @var bool
     */
    public $withoutActionEvents = true;

    /**
     * The displayable name of the action.
     *
     * @var string
     */
    public $name = 'Обработать через API';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        if ($models->count() < 1){
            return Action::danger('Нет файлов с сотрудниками');
        }
        $telegramAccounts = TelegramAccount::query()->where('session_exist', 1)->get();
        if ($telegramAccounts->count() < 2){
            return Action::danger('Вы не создали сессии для всех аккаунтов телеграмма!');
        }

//запускает выполнение акшена который выполняет парсинг и обработку результатов с записью их в бд.
// при большом объеме данных для парсинга, рекомендуется воспользоваться постановкой задачи в очередь.
//            foreach ($models as $model) {
//                app(TelegramParseController::class)->indexIntoApi($model->id);
//            }
//            return Action::message("Обработка прошла успешно!");

//ставим задачу на выполнение парсинга в очередь.

        foreach ($models as $model) {
            ProcessParse::dispatch($model);
        }
//        Artisan::call('queue:work --stop-when-empty', []);
        if (env('QUEUE_CONNECTION') == 'sync'){
            return Action::message("Задача выполнена!");
        } else {
            return Action::message("Задача поставлена в очередь!");
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
