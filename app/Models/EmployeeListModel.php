<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class EmployeeListModel extends Model
{
    use HasFactory;

    public $fillable = [
        'file',
    ];


    public static function getAndProccessingFile($fileName){
        $handle = fopen(Storage::path($fileName['file']), "r");
        $employeeData = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if (strpos($line, 'CN=')){
                    $oneLine = substr($line, strpos($line, 'CN=') + 3, strpos($line, ',') - 5);
                    if (strstr($oneLine, 'Все') == false){
                        $employeeData[] = explode(' ', $oneLine)[0];
                    }
                }
            }
            fclose($handle);

            return $employeeData;
        } else {
            dd('error');
        }
    }

    public static function isExeption ($str) {
        $exList = [
            'Диспетчер',
            'ул.',
            'Все',
//            '79858463842'
        ];
        foreach ($exList as $ex){
            if (strpos(mb_strtolower($str), mb_strtolower($ex)) !== false) {
                return true;
            }
        }
        return false;
    }

    public static function getAndProccessingFileWithTelNumber($fileName){
        $handle = fopen(Storage::path($fileName['file']), "r");
        $employeeData = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
             $oneEmployee = [];
                if (strpos($line, 'CN=') !== false && !self::isExeption($line)){

                    $userName = substr($line, strpos($line, 'CN=') + 3, strpos($line, ',')-5);

                    $exceptions = [
                        '(только Telegram)',
                    ];
                    foreach ($exceptions as $exception){
                        if (strpos($line, $exception) !== false){
                            $userName = str_replace($exception, '', $userName);
                            $userName = trim($userName);
                            $userName = preg_replace('/\s+/', ' ', $userName);
                        }
                    }

                    $isSimbolExist = strpos($line, '+') !== false;
                    $userPhone = $isSimbolExist ? trim(substr($line, strpos($line, '+'), 13)) : 'none';

                    if (strstr($userName, ')') == false){
                        if (array_key_exists(1, explode(' ', $userName))) {
                            $oneEmployee['firstName'] = explode(' ', $userName)[1];
                        }
                        if (array_key_exists(2, explode(' ', $userName))) {
                            $oneEmployee['middleName'] = explode(' ', $userName)[2];
                        };

                        $oneEmployee['lastName'] = explode(' ', $userName)[0];
                        $oneEmployee['phone'] = $userPhone;
                        $employeeData[] = $oneEmployee;
                    } elseif (strstr($userName, ')') !== false){
                        if (array_key_exists(2, explode(' ', $userName))) {
                            $oneEmployee['firstName'] = explode(' ', $userName)[2];
                        }
                        if (array_key_exists(3, explode(' ', $userName))) {
                            $oneEmployee['middleName'] = explode(' ', $userName)[3];
                        };

                        $oneEmployee['lastName'] = explode(' ', $userName)[0];
                        $oneEmployee['maidenName'] = str_replace( ['(', ')'], '', explode(' ', $userName)[1]);
                        $oneEmployee['phone'] = $userPhone;
                        $employeeData[] = $oneEmployee;
                    }
                }

            }
            fclose($handle);
//dd($employeeData);
            return $employeeData;

        } else {
            dd('error');
        }
    }
}
