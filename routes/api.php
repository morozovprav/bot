<?php

use App\Http\Controllers\TelegramParseController;
use App\Http\Controllers\TestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/parse', [TelegramParseController::class, 'index']);
Route::get('/parse_file', [TelegramParseController::class, 'parseFileWithTelNumber']);
Route::get( '/test', [TestController::class, 'testQuery']);
Route::match(['get', 'post'],'/api_parse', [TelegramParseController::class, 'indexIntoApi']);
