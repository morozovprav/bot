<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramSession extends Model
{
    use HasFactory;

    public $fillable = [
        'user_id',
        'session_file',
        'telegram_account_id'
    ];

    public function telegramAccount()
    {
        return $this->belongsTo(TelegramAccount::class);
    }
}
