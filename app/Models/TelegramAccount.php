<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Fields\HasOne;

class TelegramAccount extends Model
{
    use HasFactory;

    public $fillable = [
        'phone',
        'session_exist'
    ];

    public function session()
    {
        return $this->hasOne(TelegramSession::class);
    }
}
