<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMatchFieldsToTelegramParseModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table) {
            $table->dropColumn('last_name_matched');
            $table->dropColumn('first_name_matched');
            $table->dropColumn('match_lvl');
            $table->tinyInteger('full_name_matched');
            $table->string('employee_first_name')->nullable();
            $table->string('employee_middle_name')->nullable();
            $table->string('employee_last_name')->nullable();
            $table->string('employee_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telegram_parse_models', function (Blueprint $table) {
            $table->tinyInteger('last_name_matched');
            $table->tinyInteger('first_name_matched');
            $table->tinyInteger('match_lvl');
            $table->dropColumn('full_name_matched');
            $table->dropColumn('employee_first_name');
            $table->dropColumn('employee_middle_name');
            $table->dropColumn('employee_last_name');
            $table->dropColumn('employee_phone');
        });
    }
}
