<?php

namespace App\Jobs;

use App\Http\Controllers\TelegramParseController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessParse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $model;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->model = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            app(TelegramParseController::class)->indexIntoApi($this->model->id);
    }
}
