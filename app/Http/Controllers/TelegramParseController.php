<?php

namespace App\Http\Controllers;

use App\Models\EmployeeListModel;
use App\Models\TelegramAccount;
use App\Models\TelegramParseModel;
use danog\MadelineProto\API;
use danog\MadelineProto\Logger;
use danog\MadelineProto\MyTelegramOrgWrapper;
use danog\MadelineProto\Shutdown;
use Hu\MadelineProto\Facades\Factory;
use Hu\MadelineProto\Facades\MadelineProto;
use Hu\MadelineProto\Facades\Messages;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Actions\Action;


class TelegramParseController extends Controller
{

    public function index($modelId)
    {

        $telegramRawData = TelegramParseModel::getTelegramData('getUpdates')['result'];

        if (count($telegramRawData) < 2){
            return Action::danger('Нет данных из телеграм чатов!');
        }

        $data = [];
        while (count($telegramRawData) > 1){
            $data = array_merge($data, $telegramRawData);
            $updateId =  $telegramRawData[array_key_last($telegramRawData)]['update_id'];
            TelegramParseModel::getTelegramData('getUpdates', ['offset' => $updateId]);
            $telegramRawData = TelegramParseModel::getTelegramData('getUpdates')['result'];
        }

        $data = TelegramParseModel::telegramDataProcessing($data);

        $fileName = EmployeeListModel::query()->select('file')->firstOrFail();

        $employees = EmployeeListModel::getAndProccessingFile($fileName);

        $result = TelegramParseModel::writeToDb($data, $employees);

    }

    // выполняет то же что и index только все логика в модели
    public function test()
    {
        return TelegramParseModel::test();
    }

    public function telApi (Request $request)
    {

//        $settings = [
//            'app_info' => [ // Эти данные мы получили после регистрации приложения на https://my.telegram.org
//                'api_id' => '16308342',
//                'api_hash' => 'c22e44c54df1edba8d35eb0a2775804e',
//            ],
//            'logger' => [ // Вывод сообщений и ошибок
//                'logger' => 3, // выводим сообещения через echo
//                'logger_level' => 4, // выводим только критические ошибки.
//            ],
//            //для доступа может потребоваться socks5 прокси
//            //если прокси не требуется, то этот блок можно удалить.
//            'connection_settings' => [
//                'all' => [
//                    'proxy' => '\SocksProxy',
//                    'proxy_extra' => [
//                        'address' => 'xxx.xxx.xxx.xxx',
//                        'port' => 1234,
//                        'username' => '',//Можно удалить если логина нет
//                        'password' => '',//Можно удалить если пароля нет
//                    ],
//                ],
//            ],
//            'serialization' => [
//                'serialization_interval' => 300,
//                //Очищать файл сессии от некритичных данных.
//                //Значительно снижает потребление памяти при интенсивном использовании, но может вызывать проблемы
//                'cleanup_before_serialization' => true,
//            ],
//        ];


//        Messages::getAllChats();
        $r = MadelineProto::getClient();

// Получаем все чаты в которых состоит юзер
//        dd($r->messages->getAllChats());

// Получаем инфо по одному чату с юзерами(только id)
//        dd($r->getFullInfo(-684927467));

//Получаем инфо по чату и инфу по юзерам
        dd($r->getPwrChat(-684927467));




//        $MadelineProto = new \danog\MadelineProto\API('session.madeline');
//        $MadelineProto->start();
//
//        $me = $MadelineProto->getSelf();
//
//        $MadelineProto->logger($me);
//
//        if (!$me['bot']) {
//            $MadelineProto->messages->sendMessage(['peer' => '@danogentili', 'message' => "Hi!\nThanks for creating MadelineProto! <3"]);
//            $MadelineProto->channels->joinChannel(['channel' => '@MadelineProto']);
//
//            try {
//                $MadelineProto->messages->importChatInvite(['hash' => 'https://t.me/joinchat/Bgrajz6K-aJKu0IpGsLpBg']);
//            } catch (\danog\MadelineProto\RPCErrorException $e) {
//                $MadelineProto->logger($e);
//            }
//
//            $MadelineProto->messages->sendMessage(['peer' => 'https://t.me/joinchat/Bgrajz6K-aJKu0IpGsLpBg', 'message' => 'Testing MadelineProto!']);
//        }
//        $MadelineProto->echo('OK, done!');

    }

    public function getTelegramAccountsForParse($main = true) {
        $telegramAccounts = [];
        $telAccount = TelegramAccount::query()->where('session_exist', 1)->get();
        foreach ($telAccount as $item){
            if ($item->main == 1){
                $telegramAccounts['main'] = $item->session->session_file;
            } else {
                $telegramAccounts['other'] = $item->session->session_file;
            }
        }
        if ($main) {
            return $telegramAccounts['main'];
        } else {
            return $telegramAccounts['other'];
        }
    }

    public function indexIntoApi($modelId = 1) {

        $dataMainAccount = TelegramParseModel::getTelegramDataIntoApi(self::getTelegramAccountsForParse(), true);
//dd($dataMainAccount);
        $dataOtherAccount = TelegramParseModel::getTelegramDataIntoApi(self::getTelegramAccountsForParse(false));
//dd($dataOtherAccount);
        foreach ($dataOtherAccount as $key => $user){
            if($user['phone'] == 'none'){
                $dataOtherAccount[$key]['phone']  = $dataMainAccount[$user['user_id']]['phone'];
            }
        }

        $fileName = EmployeeListModel::query()->select('file')->where('id', $modelId)->firstOrFail();
        $employees = EmployeeListModel::getAndProccessingFileWithTelNumber($fileName);
        $result = TelegramParseModel::writeToDb($dataOtherAccount, $employees);
    }

    public function parseFileWithTelNumber(){
        $fileName = EmployeeListModel::query()->select('file')->firstOrFail();
        $employees = EmployeeListModel::getAndProccessingFileWithTelNumber($fileName);
    }
}
