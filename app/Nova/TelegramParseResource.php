<?php

namespace App\Nova;

use App\Models\TelegramParseModel;
use Illuminate\Http\Request;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class TelegramParseResource extends Resource
{

//    public static $group = 'Инструменты проверки';

    public static function label()
    {
        return 'Список участников чатов';
    }

    /**
     * The visual style used for the table. Available options are 'tight' and 'default'.
     *
     * @var string
     */
    public static $tableStyle = 'tight';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = TelegramParseModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        if (!$request->count) {
            $request->count = 0;
        }
        return [
            Text::make('#', function () use ($request) {
                $request->count += 1;

                $rowNumber = $request->page == 1 ? $request->count : $request->count + ($request->perPage * ($request->page - 1));
                return $rowNumber;
            })->onlyOnIndex(),
            Text::make('Id чата', 'chat_id')->hideFromIndex(),
            Text::make('Название чата', 'chat_title')->sortable(),
            Text::make('Id пользователя', 'user_id')->hideFromIndex(),
            Text::make('1 поле имени', 'first_name')->sortable(),
            Text::make('2 поле имени', 'last_name')->sortable(),
            Text::make('Ник', 'username')->hideFromIndex(),
            Text::make('Телефон', 'phone')->sortable(),
            Text::make('Телефон наш', 'phone_matched')->sortable(),
            Text::make('Фио АД', 'full_name_matched')->sortable(),
            Text::make('Фамилия(файл)', 'employee_last_name')->sortable(),
            Text::make('Девичья фамилия(файл)', 'employee_maiden_name')->sortable(),
            Text::make('Имя(файл)', 'employee_first_name')->sortable(),
            Text::make('Отчество(файл)', 'employee_middle_name')->sortable(),
            Text::make('Телефон(файл)', 'employee_phone')->sortable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)->withHeadings()->allFields()->withoutActionEvents(),
        ];
    }
}
