<?php

namespace App\Nova\Actions;

use App\Http\Controllers\TelegramParseController;
use App\Models\TelegramParseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Exceptions\NovaExceptionHandler;
use Laravel\Nova\Fields\ActionFields;

class RunParseProcessing extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Disables action log events for this action.
     *
     * @var bool
     */
    public $withoutActionEvents = true;

    /**
     * The displayable name of the action.
     *
     * @var string
     */
    public $name = 'Обработать файл через бота';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {

        if ($models->count() < 1){
            return Action::danger('Нет записей в таблице');
        }

        foreach ($models as $model) {
            app(TelegramParseController::class)->index($model->id);
        }

        return Action::message('Обработка выполнена успешно!');
//        return app(TelegramParseController::class)->test();

    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [

        ];
    }
}
